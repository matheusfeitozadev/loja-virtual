# Marketplace

Front end and Back end application for FPF exam test

- backend project

* Description: in this project, you will find a api project that providers all information about a product
* Used Technologies: Django Rest Framework(Python) and ASP .NET MVC API(C#)

- Django Rest Framework(Python)

* Step by step to run the project:
1 - Access the folder "django-api" by command "cd django-api"
2 - Run the command "py -m venv env" to create "env" folder with enviroment
3 - Run the command "env\Scripts\activate" in order to active enviroment
4 - Run the command "cd project"
5 - Run the command "py manage.py runserver" to run the project
6 - Go to browser and type end-point http://localhost:port/produto to test application


- ASP .NET MVC API(C#)

* Step by step to run the project:
1 - Open the folder "c_sharp_api/API"
2 - Open the file "API.sln" with Visual Studio installed in your machine
2 - Right click on solution project name in the left side bar, go to clean, build and rebuild
3 - Run the project
4 - Go to browser and type end-point http://localhost:port/produto to test application