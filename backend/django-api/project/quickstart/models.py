from django.db import models

class Estoque(models.Model):
    quantidade = models.IntegerField(default=0)
    preco = models.DecimalField(default=0, decimal_places=2, max_digits=6)

# Create your models here.
class Produto(models.Model):
    nome_produto = models.TextField(max_length=100)
    descricao_produto = models.TextField(max_length=100)
    foto = models.TextField(null=True)
    ativo = models.BooleanField(default=True)
    estoque = models.ForeignKey(Estoque, on_delete=models.CASCADE, null=True)