from django.contrib.auth.models import User, Group
from rest_framework import serializers
from quickstart.models import Produto, Estoque

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'username', 'email', 'groups']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

#class ProdutoSerializer(serializers.HyperlinkedModelSerializer):
#    class Meta:
#        model = Produto
#        fields = ['id', 'nome_produto', 'descricao_produto', 'foto', 'ativo']

class EstoqueSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    quantidade = serializers.IntegerField(required=True)
    preco = serializers.DecimalField(required=True, max_digits=7, decimal_places=2)

    class Meta:
        model = Estoque
        fields = ['id','quantidade', 'preco']

class ProdutoSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    nome_produto = serializers.CharField(required=True,max_length=100)
    descricao_produto = serializers.CharField(required=True, max_length=100)
    foto = serializers.CharField(allow_null=True)
    ativo = serializers.BooleanField()
    estoque = EstoqueSerializer(required=True)

    class Meta:
        model = Produto
        fields = ['id', 'nome_produto','descricao_produto','foto','ativo','estoque']

    def create(self, validated_data):
        estoque = validated_data.pop('estoque')
        produto = Produto.objects.create(**validated_data)
        produto.estoque = Estoque.objects.create(quantidade=estoque['quantidade'], preco= estoque['preco'])
        produto.save()

        return produto

    def update(self, instance, validated_data):
        instance.nome_produto = validated_data.get('nome_produto', instance.nome_produto)
        instance.descricao_produto = validated_data.get('descricao_produto', instance.descricao_produto)
        instance.foto = validated_data.get('foto', instance.foto)
        instance.ativo = validated_data.get('ativo', instance.ativo)

        estoque = validated_data.pop('estoque')
        instance.estoque.quantidade = estoque['quantidade']
        instance.estoque.preco = estoque['preco']

        instance.estoque.save()
        instance.save()

        return instance

