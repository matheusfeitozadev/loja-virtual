# Generated by Django 3.2.5 on 2021-07-09 15:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quickstart', '0003_alter_produto_foto'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='estoque',
            name='produto',
        ),
        migrations.AddField(
            model_name='produto',
            name='estoque',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='quickstart.estoque'),
        ),
    ]
