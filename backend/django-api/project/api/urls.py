from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter

from quickstart import views

router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'Groups', views.GroupViewSet)
router.register(r'Produto', views.ProdutoViewSet, basename='produto')
router.register(r'produto', views.ProdutoViewSet, basename='produto')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('', include(router.urls)),
]
