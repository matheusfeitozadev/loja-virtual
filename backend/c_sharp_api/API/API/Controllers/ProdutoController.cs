﻿using API.DAO;
using API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class ProdutoController : ApiController
    {
        private DaoProduto daoProduto;
        public ProdutoController()
        {
            daoProduto = new DaoProduto();
        }
        [HttpGet]
        public IHttpActionResult Get()
        {
            var list = daoProduto.listAll();

            return Ok(list);
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var model = daoProduto.getById(id);

            return Ok(model);
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody] ProdutoModel produto)
        {
            var model = daoProduto.add(produto);

            return Ok(model);
        }

        [HttpPatch]
        public IHttpActionResult Patch([FromBody] ProdutoModel produto)
        {
            var model = daoProduto.update(produto);

            return Ok(model);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            daoProduto.delete(id);

            return Ok();
        }
    }
}
