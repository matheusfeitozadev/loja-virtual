﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ViewModel
{
    public class ProdutoModel
    {
        public int id { get; set; }
        public string nome_produto { get; set; }
        public string descricao_produto { get; set; }
        public string foto { get; set; }
        public bool ativo { get; set; }
        public EstoqueModel estoque { get; set; }

        public ProdutoModel()
        {

        }

        public ProdutoModel(produto produto)
        {
            id = produto.id;
            nome_produto = produto.nome_produto;
            descricao_produto = produto.descricao_produto;
            foto = produto.foto;
            ativo = produto.ativo;
            estoque = new EstoqueModel(produto.estoque);
        }

        public static produto returnModelDB(ProdutoModel produtoModel)
        {
            produto produto = new produto();
            produto.nome_produto = produtoModel.nome_produto;
            produto.descricao_produto = produtoModel.descricao_produto;
            produto.foto = produtoModel.foto;
            produto.ativo = produtoModel.ativo;

            produto.estoque = new estoque();
            produto.estoque.quantidade = produtoModel.estoque.quantidade;
            produto.estoque.preco = produtoModel.estoque.preco;

            return produto;
        }

        public static produto returnModelDBUpdated(ProdutoModel produtoModel, produto produto)
        {
            produto.nome_produto = produtoModel.nome_produto;
            produto.descricao_produto = produtoModel.descricao_produto;
            produto.foto = produtoModel.foto;
            produto.ativo = produtoModel.ativo;

            produto.estoque.quantidade = produtoModel.estoque.quantidade;
            produto.estoque.preco = produtoModel.estoque.preco;

            return produto;
        }
    }
}