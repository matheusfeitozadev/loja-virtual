﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ViewModel
{
    public class EstoqueModel
    {
        public int id { get; set; }
        public int quantidade { get; set; }
        public decimal preco { get; set; }

        public EstoqueModel()
        {

        }

        public EstoqueModel(estoque estoque)
        {
            id = estoque.id;
            quantidade = estoque.quantidade;
            preco = estoque.preco;
        }
    }
}