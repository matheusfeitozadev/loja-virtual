﻿using API.Models;
using API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.DAO
{
    public class DaoProduto
    {
        private marketplaceEntities context;

        public DaoProduto()
        {
            context = new marketplaceEntities();
        }

        public List<ProdutoModel> listAll()
        {
            List<ProdutoModel> list = new List<ProdutoModel>();

            try
            {
                var listBase = context.produto.ToList();

                foreach (var item in listBase)
                {
                    list.Add(new ProdutoModel(item));
                }
            }
            catch(Exception)
            {

            }

            return list;
        }

        public ProdutoModel getById(int id)
        {
            try
            {
                var data = context.produto.FirstOrDefault(x => x.id == id);

                if (data != null)
                {
                    return new ProdutoModel(data);
                }
                else
                {
                    return null;
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        public ProdutoModel add(ProdutoModel produtoModel)
        {
            context = new marketplaceEntities();

            try
            {
                var modelDB = ProdutoModel.returnModelDB(produtoModel);

                modelDB.estoque = context.estoque.Add(modelDB.estoque);
                context.SaveChanges();

                modelDB = context.produto.Add(modelDB);
                context.SaveChanges();

                return new ProdutoModel(modelDB);
            }
            catch(Exception)
            {
                return null;
            }   
        }

        public ProdutoModel update(ProdutoModel produtoModel)
        {
            context = new marketplaceEntities();

            try
            {
                var data = context.produto.FirstOrDefault(x => x.id == produtoModel.id);

                if (data != null)
                {
                    data = ProdutoModel.returnModelDBUpdated(produtoModel, data);

                    context.SaveChanges();

                    return new ProdutoModel(data);
                }
                else
                {
                    return null;
                }
            }
            catch(Exception)
            {
                return null;
            }
        }

        public void delete(int id)
        {
            context = new marketplaceEntities();

            try
            {
                var data = context.produto.FirstOrDefault(x => x.id == id);

                if (data != null)
                {
                    context.produto.Remove(data);

                    context.SaveChanges();

                    context.estoque.Remove(data.estoque);

                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}