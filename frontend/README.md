# Marketplace

Front end and Back end application for FPF exam test

- frontend project

* Description: in this project, you will find a crud of products accessing a end-point api.
* Used Technologies: Angular

* Step by step to run the project:
1 - Access the folder "frontend" by command "cd frontend"
2 - Run the command "npm i -g @angular/cli"
3 - Run the command "npm install" to install all dependecies
4 - Run the command "ng serve" to run the project on browser