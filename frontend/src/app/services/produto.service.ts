//service used to perform crud operation via API

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProdutoModel } from '../interfaces/produto-model';

const baseUrl = 'http://localhost:3000/produto';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {
  constructor(private http : HttpClient) { }

  getAll() : Observable<ProdutoModel[]>
  {
    return this.http.get<ProdutoModel[]>(baseUrl);
  }

  get(id? : number) : Observable<ProdutoModel>
  {
    return this.http.get<ProdutoModel>(`${baseUrl}/${id}`);
  }

  post(data : ProdutoModel) : Observable<ProdutoModel>
  {
    return this.http.post<ProdutoModel>(baseUrl, data);
  }

  delete(id? : number) : Observable<any>
  {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  patch(data : ProdutoModel) : Observable<ProdutoModel>
  {
    return this.http.patch<ProdutoModel>(`${baseUrl}/${data.id}`, data);
  }
}
