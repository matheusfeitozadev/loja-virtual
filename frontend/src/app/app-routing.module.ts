import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProdutoListComponent } from './components/produto/produto-list/produto-list.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {
    path : '',
    redirectTo : 'home/',
    pathMatch : 'full'
  },
  {
    path : 'home',
    component : HomeComponent
  },
  {
    path: 'produtos',
    component : ProdutoListComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
