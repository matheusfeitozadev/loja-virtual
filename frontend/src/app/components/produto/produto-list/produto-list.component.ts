import { ProdutoModel } from './../../../interfaces/produto-model';
import { ProdutoService } from './../../../services/produto.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Action } from './../../../interfaces/action-model';
import { ConfirmDialogComponent, DialogData } from '../../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-produto-list',
  templateUrl: './produto-list.component.html',
  styleUrls: ['./produto-list.component.css']
})
export class ProdutoListComponent implements OnInit {

  //variable to list products from api
  produtos : ProdutoModel[] = [];

  registerForm: FormGroup;
  submitted = false;

  //variable to be used in add/edit users
  produto : ProdutoModel = {
    id : 0,
    nome_produto : "",
    descricao_produto : "",
    foto : "",
    ativo : true,
    estoque : {
      id : 0,
      quantidade : 0,
      preco : 0
    }
  };

  //variables to display columns
  displayedColumns: string[] = ['id', 'nome', 'descricao', 'foto', 'ativo', 'quantidade', 'preco', 'edit', 'delete'];
  
  //variables to be used to show divs add and edid
  isList : boolean = false;
  isAdd : boolean = false;
  isEdit : boolean = false;
  textHead : string = "";

  constructor(
    private produtoService : ProdutoService, 
    private router : Router, 
    private dialog : MatDialog,
    private formBuilder: FormBuilder) {
      this.createFormGroup();
    }

  ngOnInit(): void {
    

    this.setActionPage(Action.list);
  }

  createFormGroup()
  {
    this.registerForm = this.formBuilder.group({
        nome_produto: new FormControl(this.produto.nome_produto, [Validators.required]),
        descricao_produto: new FormControl(this.produto.descricao_produto, [Validators.required]),
        quantidade:  new FormControl(this.produto.estoque.quantidade, [Validators.required, Validators.min(1)]),
        preco:  new FormControl(this.produto.estoque.preco, [Validators.required, Validators.min(1)]),
    });
  }

  get f() { return this.registerForm.controls; }

  //function to list all products from api
  listAllProdutos() {
    this.produtoService.getAll().subscribe(
      data => {
        this.produtos = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  //function to add(post) and edit(patch) products to api
  addOrEdit()
  {
    if (this.registerForm.invalid) {
        return;
    }

    this.produto.nome_produto = this.registerForm.get('nome_produto')?.value;
    this.produto.descricao_produto = this.registerForm.get('descricao_produto')?.value;
    this.produto.estoque.quantidade = this.registerForm.get('quantidade')?.value;
    this.produto.estoque.preco = this.registerForm.get('preco')?.value;

    if(this.isAdd)
    {
        this.produtoService.post(this.produto).subscribe(
          (data)=>{
            this.clearProduto();
            this.goBack();
          },
          error => {
            console.log(error);
          }
        )
    }
    else{
      this.produtoService.patch(this.produto).subscribe(
        (data)=>{
          this.clearProduto();
          this.goBack();
        },
        error => {
          console.log(error);
        }
      )
    }
  }

  //function to list products and div with table
  goBack()
  {
    this.setActionPage(Action.list);
  }

  //function to show div add product
  goToAddNew() {
    this.clearProduto();
    this.setActionPage(Action.add);
  }

  //function to show div edit product and fill the fields
  goToEdit(produto : ProdutoModel)
  {
    this.produto = produto;
    this.createFormGroup();
    this.setActionPage(Action.edit);
  }

  //function to show modal alert if user want to delete
  askDelete(produto : ProdutoModel)
  {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: {
          title: "Are you sure?",
          message: "You will delete this product"}
    });
  
    dialogRef.afterClosed().subscribe(dialogResult => {
      if(dialogResult)
      {
        this.produtoService.delete(produto.id).subscribe(
          data =>{
            this.clearProduto();
            this.goBack();
          },
          error =>{
            console.log(error);
          }
        )
      }
   });
  }

  //function to manage and perform which div will be showed
  setActionPage(action : Action)
  {
      if(action == Action.add)
      {
          this.isList = false;
          this.isAdd = true;
          this.isEdit = false;
      }
      else{
        if(action == Action.edit)
        {
            this.textHead = "Edit Product";
            this.isList = false;
            this.isAdd = false;
            this.isEdit = true;
        }
        else{
          if(action == Action.list)
          {
              this.listAllProdutos();
              this.textHead = "New Product";
              this.isList = true;
              this.isAdd = false;
              this.isEdit = false;
          }
        }
      }
  }

  //clear all information from variable which reperesents actual product
  clearProduto()
  {
      this.produto.id = 0;
      this.produto.nome_produto = "";
      this.produto.descricao_produto = "";
      this.produto.foto = "";
      this.produto.ativo = true;
      this.produto.estoque.id = 0;
      this.produto.estoque.quantidade = 0;
      this.produto.estoque.preco = 0;
  }

}
