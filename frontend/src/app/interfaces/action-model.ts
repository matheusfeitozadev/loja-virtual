
//enum to identify action on component
export enum Action{
    list = 1,
    add = 2,
    edit = 3
} 