
//interface to products crud
export interface ProdutoModel {
    id? : number,
    nome_produto : string,
    descricao_produto : string,
    foto? : string,
    ativo : boolean,
    estoque : {
        id? : number,
        quantidade : number,
        preco : number
    }
}
